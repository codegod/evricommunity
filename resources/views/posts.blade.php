@extends('layouts.app')

@section('content')

@foreach ($posts as $post)

<div class="bwrapsite-content" id="bwrapcontent" >
  <div class="post-outer">
      <article class="post hentry">
        <header class="entry-header">
          <h2 class="post-title entry-title">
            {{ $post->subject }}
          </h2>
        </header>
        <div class="post-header-line-1">
        </div>
        <div class="post-body entry-content">
          <div>
            <img src="http://3.bp.blogspot.com/-xUiI8Gt_QyM/VmgGaFQcp3I/AAAAAAAAIrY/pmC0lS28CWI/s1600/btemplates-aquarium-by-francisco-oliveros.jpg" class="pbtthumbimg">
            <div>
            {{ $post->description }} 
            </div>
          </div>
          <div style="float:right;padding-right:10px;margin-top:10px;">
            <a class="nbtmore-link" style="color:#111;" href="{{ route('posts.show', $post->id) }}">Read More</a>
          </div>
          <div style="clear: both;"></div>
        </div>
        <footer class="nbtentry-meta">
          <span >
            <i class="fa fa-clock-o" aria-hidden="true"></i>
          {{ $post->created_at }}
          </span>
          <span class="nbtbyline">
            <span>
              <a href="#" rel="author" title="author profile">
                {{ $post->user->name }}
              </a>
            </span>
          </span>
          <span class="nbttags-links">
            <a href="http://sorbet2-btemplates.blogspot.md/search/label/Blogger" rel="tag">
            Blogger
            </a>, 
            <a href="http://sorbet2-btemplates.blogspot.md/search/label/Images" rel="tag">
            Images
            </a>,
            <a href="http://sorbet2-btemplates.blogspot.md/search/label/Tag" rel="tag">
            Tag
            </a>
          </span>
          <span class="nbtcomments-link">
            <a style="color: #9ba2af;" href="{{ route('posts.show', $post->id) }}">
            {{ $post->category_id }} comments
            </a>
          </span>
        </footer>
      </article>
    </div>
</div>

@endforeach

@endsection


@section('includes')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/css/post.css">
  <style type="text/css">
.stretchedToMargin {
     bottom: auto;
   }
     a:visited{
      color: #fff;
     }
  </style>
@endsection