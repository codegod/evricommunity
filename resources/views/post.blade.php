@extends('layouts.app')

@section('content')

<div class="bwrapsite-content" id="bwrapcontent" >



  <div class="post-outer">
      <article class="post hentry">
        <header class="entry-header">
          <h2 class="post-title entry-title">
           {{$post->subject}}
          </h2>
        </header>
        <div class="post-header-line-1">
        </div>
        <div class="post-body entry-content">
          <div>
            <img src="/images/back3.jpg" class="pbtthumbimg">
            <div>
            {{ $post->description }}
            </div>
          </div>
          <div style="clear: both;"></div>
        </div>
        <footer class="nbtentry-meta">
         <div class="comments" id="comments">
<a name="comments"></a>
<h4>comments:</h4>

<table>
@foreach ($comments as $comment)
	<tr style="min-height:40px;">
		<td width="20%">{{ $comment->user->name }}</td>
		<td>{{ $comment->description }}</td>
	</tr>
@endforeach
</table>



<div class="comment-form">
 <ul class="social-icons">
        <li><a href="http://www.facebook.com/sharer/sharer.php?u=evricommunity.com" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/intent/tweet?text={{ $post->description }}&url=evricommunity.com&via=TWITTER-HANDLER" class="social-icon"> <i class="fa fa-twitter"></i></a></li>

    </ul>

<form action="/posts" method="POST" name="commentForm" id="commentForm" accept-charset="utf-8">
{{ csrf_field() }}
<div id="commentsHolder">
<div class="commentBodyContainer">
<textarea name="description" rows="4" cols="50" id="commentBodyField" placeholder="Enter your comment..."></textarea>
<br>
</div>
<input type="hidden" name="user_id" value="{{ $post->user->id }}">
<input type="hidden" name="post_id" value="{{ $post->id }}">

<div id="identityControlsHolder" class="identity-type-CURRENT">
<div class="loggedInAvatar">
<div class="avatar-image-container avatar-stock">
</div>
</div>

</div>
</div>


<input type="submit" id="postCommentSubmit" name="postCommentSubmit" value="Publish">
<span id="empty-error"></span><span id="body-error">
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
</span>
</form>

</div>

        </footer>
      </article>
    </div>
</div>

@endsection


@section('includes')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/css/post.css">
  <link rel="stylesheet" type="text/css" href="https://www.blogger.com/static/v1/v-css/2218177523-cmtfp.css">
  <style type="text/css">
.pbtthumbimg {

width:auto;
height:auto;
}
.bwrapsite-content {
    margin: 81px auto 0;
    max-width: none;
}
#commentsHolder {
    padding: 0;
}
button, input[type="button"]{
	padding: 0;
}
#postCommentSubmit {
    height: 30px;
    padding: 6px;
    border-radius: 4px;
    }
.comments table{
	text-transform: none;
}
 a:visited{
      color: #fff;
     }

 @import url(http://fonts.googleapis.com/css?family=Lato);
@import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css);


social-icon {

    color: #fff;
}
ul.social-icons {
   text-align:right;	
}
.social-icons li {
    display: block;
   
}
.social-icons a {
    color: #fff;
    text-decoration: none;
}
.fa-facebook {
    padding:10px 14px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
	background-color:#000;
    
}
.fa-facebook:hover {
    background-color: #3d5b99;
}
.fa-twitter {
    padding:10px 11px;
    -o-transition:.5s;
    -ms-transition:.5s;
    -moz-transition:.5s;
    -webkit-transition:.5s;
    transition: .5s;
background-color:#000;
    
}
.fa-twitter:hover {
    background-color: #00aced;
}
  </style>
@endsection