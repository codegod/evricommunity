

@extends('layouts.app')

@section('content')

<div style="clear: both; text-align: center; margin-top: 100px; padding-bottom: auto">
        <p>
            Help to be helped...
        </p>
        <button class="myButton" onclick="parent.location='{{ url('/login') }}'">I have a problem</button>
    </div>
@endsection

@section('includes')
<link rel="stylesheet" type="text/css" href="css/first_page_style.css">
@endsection