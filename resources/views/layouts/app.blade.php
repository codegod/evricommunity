<!DOCTYPE html>
<html>
    <head>
         <title>Evricommunity</title>
         <link rel=icon href=images/head_logo.png sizes="16x16" type="image/png">
        <meta charset="utf-8">
        <link rel="stylesheet" href="/css/signup.css" type="text/css">
        <meta name="csrf-token" content="{{ csrf_token() }}">
@yield('includes')
    </head>
    <body>
        <div class="stretchedToMargin">
            <ul class="navbar">
                <li class="float-left"><a href="#" class="navbar-content">&nbsp</a></li>
                <li class="float-left" class="navbar-content"><a href="{{ url('/') }}">Home</a></li>
                @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                        @else
                            <li><a href="#" >{{ Auth::user()->name }} </a></li>
                                    <li>
                                        <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                        @endif
                <li class="float-right" class="navbar-content"><a href="faq.html">Q&A</a></li>
                <li class="float-right" class="navbar-content"><a href="{{ url('/about') }}">About us</a></li>
                <li class="float-right" class="navbar-content"><a href="{{ url('/posts') }}">Forum</a></li>
            </ul>

 @yield('content')
</div>
</body>
</html>
