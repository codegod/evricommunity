

@extends('layouts.app')

@section('content')

<div class="container">  
  <form id="contact" method="POST" action="{{ url('/register') }}">
    <h3>Registration</h3>
    <h4>Enter information about you</h4>
    {{ csrf_field() }}
    <fieldset>
      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name/Surname">
    </fieldset>
    <fieldset>
    <select name = "gen" required>
      <option selected disabled hidden>Your gender</option>
      <option value="F">Female</option>
      <option value="M">Male</option>
    </select>
    </fieldset>
    <fieldset>
      <input placeholder="Your Email Address" type="email" name="email" value="{{ old('email') }}" required>
    </fieldset>
    <fieldset>
      <input placeholder="Birth Date" type="date" name="birth_date" required>
    </fieldset>
    <h4>Address</h4>
    <fieldset>
      <input placeholder="Country" type="text" name="address" required>
    </fieldset>
    <h4>Password</h4>
    <fieldset>
      <input placeholder="Password" type="password" name="password" required>
    </fieldset>
    <fieldset>
      <input placeholder="Enter password again" type="password" name="password_confirmation" required>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit">Submit</button>
    </fieldset>
  </form>
</div>

@endsection